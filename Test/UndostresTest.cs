using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Firefox;
using Report;
using PageActions;

namespace UndostresTests {
    [TestFixture]
    public class UndostresTests: TestReport {
        private IWebDriver _driver;
        private HomePageActions _homePage;
        private PaymentPageActions _paymentPage;
        private LoginPageActions _loginPage;
        private TestData _testData;
        private WebDriverWait _driverWait;

        [OneTimeSetUp]
        public void Initialize () {
            //To obtain the current solution path/project path
            string path = System.Reflection.Assembly.GetCallingAssembly().Location;
            string actualPath = path.Substring(0, path.LastIndexOf("bin"));
            string projectPath = new Uri(actualPath).LocalPath;
            _driver = new ChromeDriver ();
            // _driver = new FirefoxDriver(); // Uncomment if you want to execute it in firefox
            _homePage = new HomePageActions (_driver);
            _paymentPage = new PaymentPageActions (_driver);
            _loginPage = new LoginPageActions (_driver);
            _driver.Manage ().Window.Maximize ();
            _testData = new TestData ();
            _driver.Url = _testData.Url;
            _driverWait = new WebDriverWait (_driver, TimeSpan.FromSeconds (_testData.GlobalTimeout));
            
        }


        // This Test Verifies That Payment screen is coming successfully or not
        [TestCase ("Telcel","8465433546","$10 (Recarga Saldo)"), Order (1)]
        public void ClickSiguiente_EnterOperadorAndNumeroDeCelularAndMontoDeRecarga_VerifyPaymentScreen (string operador, string numeroDeCelular, string montoDeRecarga) {
            _homePage.EnterNumeroDeCelular(_driver, numeroDeCelular);
            _homePage.EnterOperador (_driver, operador);
            _homePage.EnterMontoDeRecarga(_driver, montoDeRecarga);
            _homePage.ClickSiguiente(_driver);
            Assert.AreEqual("https://prueba.undostres.com.mx/payment.php",_driver.Url);
        }

        [TestCase ("4111111111111111", "11", "25", "111", "test@test.com", "automationexcersise@test.com", "123456"), Order (2)]
        public void SubmitPaymentDetails_CorrectPaymentDetails_SuccessfulRecharge (string cardNumber, string month, string year, string cvv, string email, string username, string password) {
            _paymentPage.SelectTarjetaAndFillDetails(_driver,cardNumber,month,year,cvv,email);
            _paymentPage.ClickTarjetaButton(_driver);
            _loginPage.Login (_driver, username, password);
        }

        [OneTimeTearDown]
        public void CloseResources () {
            TestReport.EndReport (_driver);
            _driver.Close ();
            _driver.Quit();
            TestReport._extentReport.Flush ();
        }
    }
}