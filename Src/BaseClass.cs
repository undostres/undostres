using System;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
namespace Report {
    /// <summary>Class <c>TestReport</c> used to contain all those methods which is used by every test class.It is used to generate test report for all test cases.</summary>
    /// <summary>Instance variable <c>_extentTest</c> represents the test method name for which report stores all log information.</summary>
    /// <summary>Instance variable <c>_htmlReporter</c>used to store all the information related to location where report should be store.</summary>
    /// <summary>Instance variable <c>_extentReport</c> used to write all data in test report.</summary>
    public class TestReport {
        private static ExtentTest _extentTest;
        private static ExtentHtmlReporter _htmlReporter;
        private string _folderName = "TestReports/";
        public static ExtentReports _extentReport;
        ///<summary>method <c>StartReport</c>used for loading the process for generating Test Report</summary>
        [OneTimeSetUp]
        public void StartReport () {
            if (_extentReport == null) {
                var date = DateTime.Now.ToString ("ddMMyyyyhhmmss");
                _htmlReporter = new ExtentHtmlReporter ("../../../../UndoStres/" + _folderName +"/index"+date+".html");
                _extentReport = new ExtentReports ();
                _extentReport.AttachReporter (_htmlReporter);
                _extentReport.AddSystemInfo ("Browser", "Google Chrome");
                _extentReport.AddSystemInfo ("Operating System", "Windows10");
                _extentReport.AddSystemInfo ("Host Name", "Selenium");
                _extentReport.AddSystemInfo ("User Name", " Heena Vasdani");
            }
        }

        [SetUp]
        public void SetUp () {
            _extentTest = _extentReport.CreateTest (TestContext.CurrentContext.Test.Name);
        }
        ///<summary>method <c>EndReport</c>used to store the complete result of a test into test report.</summary>
        ///<summary>Local Variable status represents the status of Tested Method.</summary>
        ///<summary>Local Variable testMessage represents the message of result of a test case.</summary>
        ///<summary>Local Variable logStatus represents the status of log.</summary>
        public static void EndReport (IWebDriver driver) {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var testMessage = TestContext.CurrentContext.Result.Message;
            Status logStatus;
            switch (status) {
                case TestStatus.Failed:
                    logStatus = Status.Fail;
                    _extentTest.Log (logStatus, "Test is ended with" + logStatus + " " + testMessage);
                    _extentTest.Log (logStatus, "Screenshot of last screen is" + _extentTest.AddScreenCaptureFromPath (TakeScreenshot (driver)));
                    break;
                case TestStatus.Passed:
                    logStatus = Status.Pass;
                    _extentTest.Log (logStatus, "Test ended with" + logStatus + " " + testMessage);
                    break;
                case TestStatus.Skipped:
                    logStatus = Status.Skip;
                    _extentTest.Log (logStatus, "Test ended with" + logStatus + " " + testMessage);
                    break;
                default:
                    logStatus = Status.Info;
                    _extentTest.Log (logStatus, "Test ended with" + logStatus + " " + testMessage);
                    break;
            }
        }
        ///<summary>method <c>TekeScreenshot</c>used to record the screenshot in failure case.</summary>
        ///<summary>Local Variable screenshot represents the screenshot of end screen in case of failure test case.</summary> 
        /// <returns>It returns a variable of Screenshot which represents the screenshot of end screen.</returns>
        public static string TakeScreenshot (IWebDriver driver) {
            Screenshot screenshot = ((ITakesScreenshot) driver).GetScreenshot ();
            var date = DateTime.Now.ToString ("ddMMyyyyhhmmss");
            string pathOfFile ="../../../../Undostres/images/" + "Screenshot" + date + ".png";
            screenshot.SaveAsFile (pathOfFile);
            return pathOfFile;
        }

    }
}