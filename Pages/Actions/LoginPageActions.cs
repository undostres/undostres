using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using PageModels;
using System;
using System.Threading;
using SeleniumExtras.WaitHelpers;

namespace PageActions
{
    public class LoginPageActions
    {
        private LoginPage _loginPageModel;
        private WebDriverWait _driverWait;
        private TestData _testData;
        public LoginPageActions(IWebDriver driver)
        {
            _loginPageModel = new LoginPage();
            _testData = new TestData();
            _driverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(_testData.GlobalTimeout));
        }

        public void Login(IWebDriver driver, string email, string password)
        {
            IWebElement loginFields = _driverWait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(_loginPageModel.loginFields)));
            driver.FindElements(By.CssSelector(_loginPageModel.loginFields))[0].Click();
            driver.FindElement(By.Id(_loginPageModel.EmailTextBox)).SendKeys(email);
            driver.FindElements(By.CssSelector(_loginPageModel.loginFields))[1].Click();
            driver.FindElement(By.Id(_loginPageModel.PasswordTextBox)).SendKeys(password);
            Thread.Sleep(10000);
            driver.FindElement(By.CssSelector(_loginPageModel.LoginButton)).Click();
        }
    }
}