using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using PageModels;
using SeleniumExtras.WaitHelpers;
using System;

namespace PageActions
{
    public class PaymentPageActions
    {
        private PaymentPage _paymentPageModel;
        private WebDriverWait _driverWait;
        private TestData _testData;
        public PaymentPageActions(IWebDriver driver)
        {
            _paymentPageModel = new PaymentPage();
            _testData = new TestData();
            _driverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(_testData.GlobalTimeout));
        }

        public void SelectTarjetaAndFillDetails(IWebDriver driver, string cardNumber, string month, string year, string cvv, string email)
        {
            driver.FindElement(By.CssSelector(_paymentPageModel.TarjetaTab)).Click();
            IWebElement nuevaTarjetaElement = _driverWait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(_paymentPageModel.NuevaTarjeta)));
            IJavaScriptExecutor JavascriptExecutor = (IJavaScriptExecutor)driver;
            JavascriptExecutor.ExecuteScript("arguments[0].click();", nuevaTarjetaElement);
            nuevaTarjetaElement.Click();
            driver.FindElement(By.Id(_paymentPageModel.CardNumber)).SendKeys(cardNumber);
            driver.FindElement(By.CssSelector(_paymentPageModel.Month)).SendKeys(month);
            driver.FindElement(By.CssSelector(_paymentPageModel.Year)).SendKeys(year);
            driver.FindElement(By.CssSelector(_paymentPageModel.CVV)).SendKeys(cvv);
            driver.FindElement(By.CssSelector(_paymentPageModel.Email)).SendKeys(email);
        }

        public void ClickTarjetaButton(IWebDriver driver){
            driver.FindElement(By.CssSelector(_paymentPageModel.TarjetaButton)).Click();
        }
    }
}