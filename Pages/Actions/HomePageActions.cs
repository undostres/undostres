using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using PageModels;
using SeleniumExtras.WaitHelpers;
using System;
using System.Threading;

namespace PageActions
{
    public class HomePageActions
    {
        private HomePage _homePageModel;
        private WebDriverWait _driverWait;
        private TestData _testData;
        public HomePageActions(IWebDriver driver)
        {
            _homePageModel = new HomePage();
            _testData = new TestData();
            _driverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(_testData.GlobalTimeout));
        }

        public void EnterOperador(IWebDriver driver, string operador)
        {
            driver.FindElement(By.CssSelector(_homePageModel.Operator)).Click();
            driver.FindElement(By.CssSelector(_homePageModel.selectedOperator+"'"+operador+"']")).Click();
        }

        public void EnterNumeroDeCelular(IWebDriver driver, string mobile)
        {
            driver.FindElement(By.CssSelector(_homePageModel.MobileNumber)).SendKeys(mobile);
        }

        public void EnterMontoDeRecarga(IWebDriver driver, string amount)
        {
            IWebElement amountElement = _driverWait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(_homePageModel.Amount+"'"+amount+"']")));
            IJavaScriptExecutor JavascriptExecutor = (IJavaScriptExecutor)driver;
            JavascriptExecutor.ExecuteScript("arguments[0].click();", amountElement);
        }

        public void ClickSiguiente(IWebDriver driver)
        {
            driver.FindElement(By.CssSelector(_homePageModel.Siguiente)).Click();
            Thread.Sleep(10000);
        }
    }
}