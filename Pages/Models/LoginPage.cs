namespace PageModels
{
    public class LoginPage
    {
        public string EmailTextBox="usrname";
        public string loginFields="#loginForm>.field.form-group";
        public string PasswordTextBox="psw";
        public string LoginButton="button[name='loginbtn']";
    }
}