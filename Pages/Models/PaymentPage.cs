namespace PageModels
{
    public class PaymentPage
    {
        public string TarjetaTab= "p[data-qa='tarjeta-tab']";
        public string NuevaTarjeta="//span[contains(text(),'Usar nueva tarjeta')]";
        public string CardNumber="cardnumberunique";
        public string Month="input[data-qa='mes-input']";
        public string Year="input[data-qa='expyear-input']";
        public string CVV="input[data-qa='cvv-input']";
        public string Email=".form-control.email";
        public string TarjetaButton=".buttonPayment.pay4";
    }
}